1. SELECT customerName
FROM customers
WHERE country = "Philippines";

2. SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = "La Rochelle Gifts";

3. SELECT productName, MSRP
FROM products
WHERE productName = "The Titanic";

4. SELECT firstName, lastName
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

5. SELECT customerName
FROM customers
WHERE state IS NULL;

6. SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson" AND firstName = "Steve";  


7. SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA" AND creditLimit < 3000;


8. SELECT customerName
FROM customers
WHERE customerName NOT LIKE '%a%';

9. SELECT customerNumber
FROM orders
WHERE comments LIKE '%DHL%';

10. SELECT productLine
FROM productlines
WHERE textDescription LIKE '%The perfect holiday%';
 

11. SELECT DISTINCT country from customers;

12. SELECT DISTINCT status from orders;

13. SELECT customerName, country
FROM customers
WHERE country = "USA" OR country = "FRANCE" OR country = "CANADA";

14. SELECT firstName, lastName
FROM employees JOIN offices
ON employees.officeCode = offices.officeCode; 

16. SELECT customerName
FROM customers JOIN employees
WHERE customers.salesRepEmployeeNumber = employees.employeeNumber;

17. SELECT lastName, firstName, customerName, offices.country AS officeCountry
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices
ON employees.officeCode = offices.officeCode
JOIN orders
ON customers.customerNumber = orders.customerNumber
WHERE customers.country = offices.country
AND orders.orderNumber IS NOT NULL;


18. SELECT lastName, firstName
FROM employees
WHERE reportsTo = 1143;


19. SELECT productName, MSRP
FROM products
WHERE MSRP = (SELECT MAX(MSRP) FROM products);


20. SELECT count(customerName) AS customersFromUK
FROM customers
WHERE country = "UK";


21. SELECT productLine, COUNT(*) AS Total
FROM products
GROUP BY productLine;


22. SELECT salesRepEmployeeNumber, COUNT(customerName)
FROM customers
WHERE salesRepEmployeeNumber IS NOT NULL
GROUP BY salesRepEmployeeNumber;

SELECT COUNT(customerName)
FROM customers
WHERE salesRepEmployeeNumber IS NOT NULL;


23. SELECT productName, quantityInStock
FROM products
WHERE productLine = "Planes"
AND quantityInStock < 1000;
 


